# PostgresSqitch

_**A docker image containing Postgres and Sqitch**_

## Contents

* [License](#license)
* [Support](#support)

## License

This project is protected by a 'Rights Reserved" license.

![License](https://img.shields.io/badge/License-Rights%20Reserved-red.svg)


## Support

The best way to show that there is a problem with this project is to submit an issue report [here](https://gitlab.com/shaybrynessynoptic/postgres-sqitch/-/issues). 
Make sure to give as much detail as possible, fully submitting all errors and how this error was achieved.

If you are consistently having issues setting up and using the project, do not hesitate to send me a private message, or email at shay.brynes@cgi.com.
